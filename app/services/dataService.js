(function() {
    //'use strict';
    angular
        .module('activity')
        .factory('dataService', dataService);

    dataService.$inject = ['$q','$http','$state']

    function dataService($q,$http,$state) {
        return {
            getActivityData: function() {
                var deferred = $q.defer();
                $http.get('static/activity-data.json').then(function(data){
                   deferred.resolve(data);
                },function(error){
                    deferred.reject();
                });
                return deferred.promise;
            }
        }
    }

})();
