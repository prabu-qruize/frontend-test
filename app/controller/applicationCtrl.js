(function() {
    'use strict';
    angular
        .module('activity')
        .controller('applicationCtrl', applicationCtrl);

    applicationCtrl.$inject = ['$scope', '$rootScope', '$state', 'dataService']

    function applicationCtrl($scope, $rootScope, $state, dataService) {
        $rootScope.menuDisplay = true;

        $rootScope.toggleMenu = function(){
            $rootScope.menuDisplay = !$rootScope.menuDisplay;
            console.log($rootScope.menuDisplay);
        }
    }

})();
