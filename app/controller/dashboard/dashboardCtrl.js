(function() {
    //'use strict';
    angular
        .module('activity')
        .controller('dashboardCtrl', dashboardCtrl);

    dashboardCtrl.$inject = ['$scope','$state','dataService']

    function dashboardCtrl($scope, $state, dataService) {
      //Get Activity Data
      $scope.linelabel = [];
      $scope.linedata = [];
      $scope.labels = [];
      $scope.data = [];
      $scope.colors = ['#c25991', '#f9992e', '#5ddfca', '#a578f7'];
      $scope.doughnut_label = [];
      $scope.doughnut_data = [];

      dataService.getActivityData().then(function(data){
         $scope.activity_data = data.data;
         for(var i in $scope.activity_data){
            //Chart Speed
            $scope.linelabel.push(moment.unix($scope.activity_data[i].data.time).format("HH:mm"))
            $scope.linedata.push($scope.activity_data[i].data.speed);
            // $scope.options.lineChart.legend
            //Chart Speed Average
            $scope.doughnut_label.push($scope.activity_data[i].zoneId);
            $scope.doughnut_data.push($scope.activity_data[i].data.speed/$scope.activity_data[i].data.count);
            //Chart Count
            $scope.labels.push($scope.activity_data[i].zoneId);
            $scope.data.push($scope.activity_data[i].data.count);
         }
         var theHelp = Chart.helpers;
         $scope.options = {
             lineChart: {legend: {
                display: true, position: "right", labels: {
                    generateLabels: function(chart) {
                        var data = chart.data;
                        if (data.labels.length && data.datasets.length) {
                            return data.labels.map(function(label, i) {
                            var meta = chart.getDatasetMeta(0);
                            var ds = data.datasets[0];
                            var arc = meta.data[i];
                            var custom = arc && arc.custom || {};
                            var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
                            var arcOpts = chart.options.elements.arc;
                            var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                            var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                            var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
                              return {
                              // And finally :
                              text:  label,
                              fillStyle: fill,
                              strokeStyle: stroke,
                              lineWidth: bw,
                              hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                              index: i
                            };
                            });
                        }
                    }
                }
             }},
             barChart: {legend: {display: true, position: "right", labels: {
                 generateLabels: function(chart) {
                     var data = chart.data;
                     if (data.labels.length && data.datasets.length) {
                         return data.labels.map(function(label, i) {
                         var meta = chart.getDatasetMeta(0);
                         var ds = data.datasets[0];
                         var arc = meta.data[i];
                         var custom = arc && arc.custom || {};
                         var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
                         var arcOpts = chart.options.elements.arc;
                         var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                         var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                         var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
                           return {
                           // And finally :
                           text:  label,
                           fillStyle: fill,
                           strokeStyle: stroke,
                           lineWidth: bw,
                           hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                           index: i
                         };
                         });
                     }
                 }
             }
             }},
             doughnutChart: {legend: {display: true,position: "bottom"}}
         };
      });

    }

})();
